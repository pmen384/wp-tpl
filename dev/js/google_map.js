window.onload = function () {
	initialize();
}
// 地図
function initialize() {
	var latlng = new google.maps.LatLng(35.001711, 135.759875); //1. 座標変更
	var myOptions = {
		zoom: 16,
		/*拡大比率*/
		zoomControl: false,
		/* 拡大コントロール */
		streetViewControl: false,
		/* ストリートビューコントロール */
		mapTypeControl: false,
		/* 左上の白い枠 */
		center: latlng,
		/*表示枠内の中心点*/
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		/*表示タイプの指定*/
		scrollwheel: false /*マウススクロールの禁止*/
	};
	var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
	/*アイコン設定▼*/
	var icon = {
		url : 'http://ohgaki-ji.com/wp-content/uploads/2016/07/map_icon.png', //2. アイコン変更
		scaledSize : new google.maps.Size(68, 76) //3. アイコンサイズ変更
	}
	var markerOptions = {
		position: latlng,
		map: map,
		icon: icon,
		title: '大垣クリニック' //4. タイトル変更
	};
	var marker = new google.maps.Marker(markerOptions);
	/*スタイルのカスタマイズ*/
	var styleOptions = [
		{
			"featureType": "all",
			"stylers": [
				{
					"hue": "#60a3fc"
				},
				{
					"lightness": "-8"
				},
				{
					"saturation":"-58"
				},
				{
					"gamma":"1.5"
				}
			]
		}
	];
	var styledMapOptions = {
		name: '大垣クリニック' //5. オプション名変更
	}
	var sampleType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
	map.mapTypes.set('sample', sampleType);
	map.setMapTypeId('sample');
}