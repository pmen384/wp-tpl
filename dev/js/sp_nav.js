/**
 * SP Navigation 
 */

jQuery( document ).ready(function($) {
  'use strict';

  $('#header #menu-button').on('click', function(){
    var menu = $(this).next('ul');
    if (menu.hasClass('open')) {
      menu.removeClass('open');
      $(this).removeClass("active");
      $(this).parent().removeClass("activeWrap");
    } else {
      menu.addClass('open');
      $(this).addClass("active");
      $(this).parent().addClass("activeWrap");
    }
  });
} );