/* gulp 本体読み込み */
var gulp = require("gulp");
/* ファイル操作 */
var fs = require('fs');
/* 置換 */
var replace = require('gulp-replace');
/* 削除 */
var del = require('del');
/* パス */
var path = require('path');
/* sass */
var sass = require("gulp-sass");
/* compass */
var compass = require("gulp-compass");
/* CSS短縮化 */
var cssmin = require("gulp-cssmin");
/* ファイル名変更 */
var rename = require("gulp-rename");
/* メディアクエリ最適化 */
var cmq = require('gulp-combine-media-queries');
/* エラーハンドリング */
var plumber = require("gulp-plumber");
/* 変更箇所だけの変更処理で無駄処理削減 */
var cache = require('gulp-cached');
/* ファイル通信 */
var sftp = require('gulp-sftp');
/* ブラウザリアルタイム反映 */
var bs = require('browser-sync').create();


/* =====================================
	CSS
===================================== */
/*--------------------------------------
	compass+min+sourcemap
--------------------------------------*/
gulp.task("css", function(done){
	gulp.src("dev/sass/**/*scss").pipe(compass({
			/* sass */
			config_file: 'config.rb',
			comments: false,
			css: 'htdocs/css',
			sass: 'dev/sass'
		}))
		/* compass */
		.pipe(cache("compass"))
		.pipe(plumber())
		/* cssmin */
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('htdocs/css/min/'))
		.on('end', function(){
			/* sourcemap */
			var pathMap = path.resolve('htdocs/css/style.css.map');
			pathMap = pathMap.replace(/\\/ig,"/");
			gulp.src(["htdocs/css/style.css"])
				.pipe(replace('style.css.map',"file:///"+pathMap))
				.pipe(gulp.dest('htdocs/css/sourcemap/'))
		});
});

/*--------------------------------------
	compass
--------------------------------------*/
gulp.task("compass", function(){
	gulp.src("dev/sass/**/*scss").pipe(compass({
			config_file: 'config.rb',
			comments: false,
			css: 'htdocs/css',
			sass: 'dev/sass'
		}))
		.pipe(plumber())
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('htdocs/css/min/'))
});

/*--------------------------------------
	cmq
--------------------------------------*/
gulp.task('cmq', function () {
	gulp.src(['htdocs/css/**/*.css','!htdocs/css/min/**/*.css','!htdocs/css/cmq/**/*.css'])
		.pipe(cmq({log: false}))
		.pipe(gulp.dest('htdocs/css/cmq/'))
});

/*--------------------------------------
	watch
--------------------------------------*/
gulp.task("wc", function(){
	gulp.watch("dev/sass/**/*scss",["css"]);
});


/* =====================================
	自動タスク
===================================== */
/*--------------------------------------
	オプション設定
--------------------------------------*/
/* JSONファイル呼び出し */
var loadJsonSync = function(filename) {
	return JSON.parse(fs.readFileSync(filename, 'utf8'));
};
/* 基本値 */
var base = {
	srcDir: 'src/', // 今回は不使用
	distDir: './htdocs/css/sourcemap'
},
conf = {
	watchPath: [
		base.distDir + '**/*.css',
	],
	sftp: {
		// サーバ情報を記載したjsonファイルのパスを指定
		server :'./sftpconfig.json',
		// sftp:allタスクを使用するときにアップする対象
		localPath: [
			base.distDir + '**'
		],
	},
	browserSync: {
		proxy: {
			proxy: 'sample.com',
			port: 9999,
			open: true,
			notify: false
		}
	}
};

/*--------------------------------------
	アップロード
--------------------------------------*/
var uploadAndReload = function(localPath, remotePath) {
	var settingFile = loadJsonSync(conf.sftp.server);
	if (remotePath) {
		settingFile.remotePath += remotePath;
	}
	gulp.task('sftp', function() {
		console.log('ファイルアップします');
		return gulp.src(localPath).pipe(sftp(settingFile));
	});
	// sftpタスクが完了してからreloadを実行する
	gulp.task('reload', ['sftp'], function() {
		console.log('リロードします');
		bs.reload();
	});
	gulp.start(['reload']);
};

/*--------------------------------------
	タスク
--------------------------------------*/
/* サーバ */
gulp.task('server', function() {
	bs.init(conf.browserSync.proxy);
});
/* 監視 */
gulp.task('watch', function() {
	gulp.watch('dev/sass/**/*scss', function(e) {
		var localPath  = e.path;
		gulp.src("dev/sass/**/*scss").pipe(compass({
			/* sass */
			config_file: 'config.rb',
			comments: false,
			css: 'htdocs/css',
			sass: 'dev/sass'
		}))
		/* compass */
		.pipe(cache("compass"))
		.pipe(plumber())
		/* cssmin */
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('htdocs/css/min/'))
		.on('end', function(){
			/* sourcemap */
			var pathMap = path.resolve('htdocs/css/style.css.map');
			pathMap = pathMap.replace(/\\/ig,"/");
			gulp.src(["htdocs/css/style.css"])
				.pipe(replace('style.css.map',"file:///"+pathMap))
				.pipe(gulp.dest('htdocs/css/sourcemap/'))
			.on('end', function(){
				uploadAndReload('./htdocs/css/sourcemap/style.css', '/');
			});
		});
	});
});
/* 全アップロード */
gulp.task('sftp:all', function() {
	var settingFile = loadJsonSync(conf.sftp.server);
	gulp.src(conf.sftp.localPath).pipe(sftp(settingFile));
});


/***************************************
	タスクの実行アーカイブ
***************************************/
gulp.task('default', ['server'], function() {
	gulp.start(['watch']);
});