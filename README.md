# WP Template

## 運用パターン
### Localhostで編集する場合
そのまま `gulpfile.js` を使用する

### Remoteで編集する場合
1. ファイル名 `gulpfile.js` を `gulpfile_local.js` に変更
2. ファイル名 `gulpfile_remote.js` を `gulpfile.js` に変更
3. `sftpconfig.json` を編集
	1. `host` の値を任意のホスト名に変更
	2. `user` の値を任意のユーザ名に変更
	3. `pass` の値を任意のパスワードに変更
	4. `remotePath` の値を任意のフルパスに変更

## 各フォルダとファイル名の説明
### deploy
本番移行関連のフォルダ。

### config.rb
sass関連の設定ファイル。

### 