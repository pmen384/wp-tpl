/* top mainimage change */
jQuery(function(){
	jQuery(window).resize(function(){
		imgChange();
	});
	imgChange();
	function imgChange() {
		jQuery(".header-image").removeAttr("width height");
		var src = jQuery(".header-image").attr("src");
		if(jQuery(window).width()<768) {
			src = src.replace("img_main.jpg","img_sp_main.jpg"); //メインイメージのパス変更
		} else {
			src = src.replace("img_sp_main.jpg","img_main.jpg"); //メインイメージのパス変更
		}
		jQuery(".header-image").attr("src",src);
	}
});

/* google analytics */