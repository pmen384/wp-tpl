/***********************************************************
		new maxheight
		600より上という数値は対応可能範囲です。任意で767などに変更可能です。
************************************************************/
jQuery(document).ready(function(){
	if(jQuery(".maxH")[0]) {
		setTimeout(function(){resizeH(".maxH")},500);
		jQuery(window).bind("orientationchange resize",function(){
			resizeH(".maxH");
		});
	}
});

/* 以下は編集しない */
function resizeH(eleH) {
	if(jQuery(window).width()>767) {
		jQuery(eleH).find(".hTit").css("height","");
		jQuery(eleH).find(".hBody").css("height","");
		jQuery(eleH).each(function(){
			var maxH = 0;
			var maxH2 = 0;
			jQuery(this).find(".hTit").each(function(){
				hTit = jQuery(this).outerHeight(true);
				if(maxH2<hTit) {
					maxH2 = hTit;
				}
			});
			jQuery(this).find(".hTit").outerHeight(maxH2);
			jQuery(this).find(".hBody").each(function(){
				hBody = jQuery(this).outerHeight(true);
				if(maxH<hBody) {
					maxH = hBody;
				}
			});
			jQuery(this).find(".hBody").outerHeight(maxH);
		});
	} else {
		jQuery(eleH).find(".hTit").css("height","");
		jQuery(eleH).find(".hBody").css("height","");
	}
}