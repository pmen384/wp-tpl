# WP移行手順

## 参考
### DNS切り替え手順
<https://wp.kaz.bz/tech/2013/04/15/1606.html>

### NSレコードを移管先と同じにしておく理由
<http://qiita.com/mickey1982/items/715fb1fa93bfa496a969>

### TTLは60にする
<http://tanaka.sakura.ad.jp/2011/03/sakura-domain-nameserver-dns-ttl.html>

---

## 手順

### 全体
1. プラグインの「**All-in-One WP Migration**」でファイルをエクスポート(<https://its-office.jp/blog/wordpress/2016/04/16/WP-Migration.html>)
2. 前準備として、metaタグ設定・リダイレクト設定をする
3. 設定＞一般＞サイトアドレス (URL)のURLを「直下(wpを消す)」に変更(※WordPress アドレス (URL)は消しちゃダメ！)
4. 　/wp/index.phpの中身の「/wp-blog-header.php」の前に「/wp」を追加
5. /wpのフォルダからindex.phpと.htaccessをダウンロード
6. 5をルートディレクトリにコピー
7. 新サーバにドメインを追加しておく
8. `dig ドメイン名` コマンドで `ANSWER SECTION` の時間を把握
9. 旧サーバのTTLを300にする
10. 旧サーバに新NSレコードを設定
11. 新サーバのネームサーバをドメイン設定で設定する
12. 反映されたかdigコマンドで確認
13. TTLをデフォルト値に戻す
14. no index,no fllowを外す
15. テーマ直下でcssパスがおかしくないか一括検索し、古いドメインが残っていれば新しいドメインに置換
16. 最終チェック

## DNS切替手順
1. `dig ドメイン名` コマンドで `ANSWER SECTION` の時間を把握
2. 旧サーバのTTLを300にする
3. 引っ越し前と引っ越し後で同じ状態にする
4. 旧サーバに新NSレコードを設定
5. 反映されたか確認
6. ドメイン設定で新サーバのネームサーバを登録(1の時間後)
7. ドメインのポリシーに応じた待ち時間（.jpなら15分）を過ぎた後、whoisなどで確認
8. TTLが切れるまで並行運用
9. 反映されたらTTLをデフォルト値に戻す