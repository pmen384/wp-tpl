/* gulp 本体読み込み */
var gulp = require("gulp");
/* ファイル操作 */
var fs = require('fs');
/* 置換 */
var replace = require('gulp-replace');
/* 削除 */
var del = require('del');
/* パス */
var path = require('path');
/* sass */
var sass = require("gulp-sass");
/* compass */
var compass = require("gulp-compass");
/* CSS短縮化 */
var cssmin = require("gulp-cssmin");
/* ファイル名変更 */
var rename = require("gulp-rename");
/* メディアクエリ最適化 */
var cmq = require('gulp-combine-media-queries');
/* エラーハンドリング */
var plumber = require("gulp-plumber");
/* 変更箇所だけの変更処理で無駄処理削減 */
var cache = require('gulp-cached');
/* ブラウザリアルタイム反映 */
var bs = require('browser-sync').create();


/* =====================================
	CSS
===================================== */
/*--------------------------------------
	compass+min+sourcemap
--------------------------------------*/
gulp.task("css", function(done){
	gulp.src("dev/sass/**/*scss").pipe(compass({
		/* sass */
		config_file: 'config.rb',
		comments: false,
		css: 'htdocs/css',
		sass: 'dev/sass'
	}))
	/* compass */
	.pipe(cache("compass"))
	.pipe(plumber())
	/* cssmin */
	.pipe(cssmin())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('htdocs/css/min/'))
	.on('end', function(){
		/* sourcemap */
		var pathMap = path.resolve('htdocs/css/style.css.map');
		pathMap = pathMap.replace(/\\/ig,"/");
		gulp.src(["htdocs/css/style.css"])
		.pipe(replace('style.css.map',"file:///"+pathMap))
		.pipe(gulp.dest('htdocs/css/sourcemap/'))
	});
});

/*--------------------------------------
	compass
--------------------------------------*/
gulp.task("compass", function(){
	gulp.src("dev/sass/**/*scss").pipe(compass({
		config_file: 'config.rb',
		comments: false,
		css: 'htdocs/css',
		sass: 'dev/sass'
	}))
	.pipe(plumber())
	.pipe(cssmin())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('htdocs/css/min/'))
});

/*--------------------------------------
	cmq
--------------------------------------*/
gulp.task('cmq', function () {
	gulp.src(['htdocs/css/**/*.css','!htdocs/css/min/**/*.css','!htdocs/css/cmq/**/*.css'])
	.pipe(cmq({log: false}))
	.pipe(gulp.dest('htdocs/css/cmq/'))
});

/*--------------------------------------
	watch
--------------------------------------*/
gulp.task("wc", function(){
	gulp.watch("dev/sass/**/*scss",["css"]);
});


/* =====================================
	サーバ
===================================== */
gulp.task('server', function() {
	bs.init({
		proxy: "http://localhost:8888"
	});
});


/* =====================================
	デフォルト
===================================== */
gulp.task('watch', function() {
	gulp.watch('dev/sass/**/*scss', function(e) {
		var localPath  = e.path;
		gulp.src("dev/sass/**/*scss").pipe(compass({
			/* sass */
			config_file: 'config.rb',
			comments: false,
			css: '../',
			sass: 'dev/sass'
		}))
		/* compass */
		.pipe(cache("compass"))
		.pipe(plumber())
		/* cssmin */
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('htdocs/css/min/'))
		.on('end', function(){
			/* sourcemap */
			var pathMap = path.resolve('../style.css.map');
			pathMap = pathMap.replace(/\\/ig,"/");
			gulp.src(["../style.css"])
				.pipe(replace('style.css.map',"file:///"+pathMap))
				.pipe(gulp.dest('htdocs/css/sourcemap/'))
			.on('end', function(){
				// uploadAndReload('./htdocs/css/sourcemap/style.css', '/');
				//gulp.task('reload', function(){
					console.log("reload now...")
					bs.reload();
				//});
				//gulp.start([reload]);
			});
		});
	});
});



/***************************************
	タスクの実行アーカイブ
***************************************/
gulp.task('default', ['server'], function() {
	gulp.start(['watch']);
});